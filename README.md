# README
Para executar a aplicação siga os seguintes passos:
1- Clone o repositório para sua máquina;
2- No terminal acessa a pasta da aplicação;
3- Execute o comando 'rake db:migrate';
4- Execute o comando "rails s";
5- Abra seu navegador e digite na barra de endereço "localhost:3000";
6- Navegue pela aplicação.